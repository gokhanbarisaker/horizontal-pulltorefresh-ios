//
//  NCViewController.m
//  HorizontalRefreshableCollection
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import "NCViewController.h"
#import "NCPlainCell.h"
#import "NCRefreshControl.h"

#define kCollectionViewCellIdentifier @"LeCell"

@interface NCViewController ()

@property (nonatomic, readwrite) NSMutableArray *dataSet;

@end

@implementation NCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Setup collection cell source
    [self.collectionView registerNib:[UINib nibWithNibName:@"NCPlainCell" bundle:nil] forCellWithReuseIdentifier:kCollectionViewCellIdentifier];
    
    // Fill data set
    self.dataSet = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", nil];
    
    
    NCRefreshControl *refreshControl = [[NCRefreshControl alloc] initWithFrame:CGRectMake(-60, 0, 60, 60)];
    refreshControl.backgroundColor = [UIColor blueColor];
    
    [refreshControl addTarget:self
                       action:@selector(refreshPrompted:)
             forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//==============================================
#pragma mark - Collection delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return self.dataSet.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LeCell";
    
    NCPlainCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UILabel *textLabel = cell.textLabel;
    
    if(textLabel)
    {
        textLabel.text = [self.dataSet objectAtIndex:indexPath.row];
    }
    
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"Selected: %d", indexPath.row);
//
//    self.successorURL = [self.avatars objectAtIndex:indexPath.row];
//}

//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"Deselected: %d", indexPath.row);
//}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = CGSizeMake(60.0f, 60.0f);
    
    return cellSize;
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(16, 16, 16, 16);
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //NSLog(@"x: %f, y: %f", scrollView.contentOffset.x, scrollView.contentOffset.y);
    
    
}

///////////////////////////////////////
#pragma mark - NCRefreshControl Bla bla

- (IBAction)refreshPrompted:(id)sender
{
    NSLog(@"Test");
    
    __weak id senderWeak = sender;
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Ending refresh");
        
        [senderWeak endRefreshing];
    });
}

@end
