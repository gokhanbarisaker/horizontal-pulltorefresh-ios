//
//  NCViewController.h
//  HorizontalPullToRefresh
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
