//
//  NCPlainCell.h
//  HorizontalRefreshableCollection
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCPlainCell : UICollectionViewCell

- (UILabel *) textLabel;

@end
