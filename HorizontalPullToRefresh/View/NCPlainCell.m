//
//  NCPlainCell.m
//  HorizontalRefreshableCollection
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import "NCPlainCell.h"

#define kPlainCellTextLabelTag 1

@implementation NCPlainCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (UILabel *) textLabel
{
    return (UILabel *) [self viewWithTag:kPlainCellTextLabelTag];
}

@end
