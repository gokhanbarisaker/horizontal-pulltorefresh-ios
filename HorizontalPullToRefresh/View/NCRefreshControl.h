//
//  NCRefreshControl.h
//  HorizontalRefreshableCollection
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, NCRefreshControlStatus)
{
    NCRefreshControlStatus_Idle = 1 << 0,    // scroll direction offset is equals, or bigger than 0
    NCRefreshControlStatus_Refreshing = 1 << 1
};

@interface NCRefreshControl : UIControl

// TODO: refreshStatus might be altered to boolean.
//@property (nonatomic, readonly) NCRefreshControlStatus refreshStatus;


- (void) beginRefreshing;
- (void) endRefreshing;
- (BOOL) isRefreshing;

@end