//
//  NCRefreshControl.m
//  HorizontalRefreshableCollection
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import "NCRefreshControl.h"

#define kScrollViewContentOffsetKey @"contentOffset"

@interface NCRefreshControl ()

@property (weak, nonatomic, readwrite) UIScrollView *observedScrollViewWeak;
@property (atomic, readwrite) BOOL currentScrollSessionCompletedRefresh;

@property (atomic, readwrite) NCRefreshControlStatus status;

@end

@implementation NCRefreshControl

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void) setup
{
//    self->_refreshStatus = NCRefreshControlStatus_Idle;
    self.status = NCRefreshControlStatus_Idle;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)didMoveToSuperview
{
    if ([self.superview isKindOfClass:[UIScrollView class]])
    {
        // Unregister from prior observed UIScrollView, if possible.
        [self unregisterAsScrollOffsetObserver];
        
        // Observe new object
        [self registerAsScrollOffsetObserver];
    }
    else
    {
        // Unregister from prior observed UIScrollView, if possible.
        
        [self unregisterAsScrollOffsetObserver];
        
        NSLog(@"Warning: Refresh control attached to non-scrollable container!");
    }
}

- (void) registerAsScrollOffsetObserver
{
    // Register as contentOffset observer
    [self.superview addObserver:self
                     forKeyPath:@"contentOffset"
                        options:NSKeyValueObservingOptionNew
                        context:NULL];
    
    // Attach weak reference of current observed object
    self.observedScrollViewWeak = (UIScrollView *) self.superview;
}

- (void) unregisterAsScrollOffsetObserver
{
    // Get strong reference of observed object
    UIScrollView *observedScrollView = self.observedScrollViewWeak;
    
    if(observedScrollView)
    {
        // Unregister observer (self) from observed object
        [observedScrollView removeObserver:self
                                forKeyPath:kScrollViewContentOffsetKey];
        
        // Clear the reference (even though it is weak, we have no use of it anymore!)
        self.observedScrollViewWeak = nil;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    // If observed value belongs to scroll content offset, 
    if ([keyPath isEqual:kScrollViewContentOffsetKey])
    {
        CGPoint contentOffset = [[change objectForKey:NSKeyValueChangeNewKey] CGPointValue];
        
        [self handleContentOffset:contentOffset];
    }
    
    // @deprecated (below code generates exception!)
    // Also, notify super class implementation
//    [super observeValueForKeyPath:keyPath
//                         ofObject:object
//                           change:change
//                          context:context];
}

- (void) handleContentOffset: (CGPoint) contentOffset
{
    // TODO: Improve the logic, so it may addapt for both horizontal and vertical scrolling
    // For now only horizontal pull to refresh is supported.
    
    CGFloat xOffset = contentOffset.x;
    
//    // If current scrolling offset towards positive direction
//    if(xOffset < .0f)
//    {
//        // Remove Idle flag
//        self.status &= ~NCRefreshControlStatus_Idle;
//    }
//    else
//    {
//        // Add Idle flag
//        self.status |= NCRefreshControlStatus_Idle;
//    }
    
    // If current scrolling offset towards positive direction
    if(xOffset >= .0f)
    {
        // Add Idle flag
        self.status |= NCRefreshControlStatus_Idle;
    }
    
    // If scrolled outside left bound
    if(xOffset < -(self.frame.size.width * .5f))
    {
        @synchronized(self)
        {
            // If no prior refresh action is active, nor performed during current scroll event
            if ([self isRefreshable])
            {
                // 
                [self beginRefreshing];
            }
            else
            {
                // Ignored...
            }
        }
    }
}

- (BOOL) isRefreshable
{
    return [self isIdle] && ![self isRefreshing];
}

- (BOOL) isRefreshing
{
    return ((self.status & NCRefreshControlStatus_Refreshing) != 0);
}

- (BOOL) isIdle
{
    return ((self.status & NCRefreshControlStatus_Idle) != 0);
}

- (void) beginRefreshing
{
    // Add refreshing flag to status
    self.status |= NCRefreshControlStatus_Refreshing;
    // Remove Idle flag
    self.status &= ~NCRefreshControlStatus_Idle;
    
    // Notify event listeners
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void) endRefreshing
{
    // Remove refreshing flag from status
    self.status &= ~NCRefreshControlStatus_Refreshing;
}

@end
