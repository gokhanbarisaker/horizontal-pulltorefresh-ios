//
//  main.m
//  HorizontalPullToRefresh
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NCAppDelegate class]));
    }
}
