//
//  NCAppDelegate.h
//  HorizontalPullToRefresh
//
//  Created by Gökhan Barış Aker on 7/8/13.
//  Copyright (c) 2013 Nomad Commerce. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NCViewController;

@interface NCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NCViewController *viewController;

@end
